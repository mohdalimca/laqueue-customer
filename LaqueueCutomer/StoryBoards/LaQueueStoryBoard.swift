//
//  LaQueueStoryBoard.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 20/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import AVKit

class LaQueueStoryBoard {
    class func Authenticate(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Authenticate", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Main(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Menu(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Menu", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Queue(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Queue", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Search(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Search", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
}
