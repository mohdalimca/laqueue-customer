//
//  QueueCell.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 07/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class QueueCell: UITableViewCell {

    @IBOutlet weak var lbl_queuename: UILabel!
    @IBOutlet weak var lbl_persons: UILabel!
    @IBOutlet weak var lbl_queueDetail: UILabel!
    @IBOutlet weak var lblQueueStatus: UILabel!
    @IBOutlet weak var lblSlot: UILabel!
    @IBOutlet weak var lblCapacity: UILabel!
    
    var btnTakePlace:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btn_takePlaceTap(_ sender: UIButton) {
        btnTakePlace?()
    }
}
