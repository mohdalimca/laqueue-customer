//
//  CategoryCell.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var view_category: UIView!
    @IBOutlet weak var lbl_catName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
