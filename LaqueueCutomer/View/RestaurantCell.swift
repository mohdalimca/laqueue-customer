//
//  RestaurantCell.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {

    @IBOutlet weak var img_restaurant: UIImageView!
    @IBOutlet weak var lbl_restName: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_watingTIme: UILabel!
    @IBOutlet weak var lbl_persons: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
