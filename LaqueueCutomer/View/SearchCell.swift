//
//  SearchCell.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var img_location: UIImageView!
    @IBOutlet weak var lbl_retailerName: UILabel!
    @IBOutlet weak var lbl_persons: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
