//
//  QueueListCell.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 26/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class QueueListCell: UITableViewCell {

    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_retailerName: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_queueTime: UILabel!
    @IBOutlet weak var lblQueueStatus: UILabel!
    var bookingStatus: BookingStatus!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(_ queue:QueueRecord) {
        lbl_status.textColor = setStatusColor(queue)
        lbl_status.text = queue.status! + "  ●"
        lbl_time.text = Date().timePassed(from: queue.bookingTime?.toDouble.dateFromTimestamp() ?? Date())
        lbl_retailerName.text = queue.queName?.capitalized ?? "--"
        lbl_date.text = queue.bookingTime?.toDouble.placeDetailDate
        lbl_queueTime.text = "\(queue.waitingTime ?? "") min"
//        let schedule = queue.schedule?.first
//        lbl_queueTime.text = "\(schedule?.oneTo ?? "") to \(schedule?.oneFrom ?? "") and \(schedule?.twoTo ?? "") to \(schedule?.twoFrom ?? "")"
        if queue.queueStatus == "Play" {
            lblQueueStatus.text = "Open"
            lblQueueStatus.textColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
        } else {
            lblQueueStatus.text = "Closed"
            lblQueueStatus.textColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
        }
    }

    private func setStatusColor(_ queue:QueueRecord) -> UIColor {
        bookingStatus = BookingStatus(rawValue: queue.status!)
        switch bookingStatus {
        case .Acknowledge:
            return #colorLiteral(red: 1, green: 0.7529411765, blue: 0, alpha: 1)
        case .Update:
            return #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
        case .Call, .Accept:
            return #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
        case .Cancel, .Noshow:
            return #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
        default: // New
            return #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
