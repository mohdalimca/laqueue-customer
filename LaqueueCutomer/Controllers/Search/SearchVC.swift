//
//  SearchVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import GoogleMaps
import GooglePlaces
import CoreLocation

class SearchVC: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var cllctn_category: UICollectionView!
    @IBOutlet weak var tblView_search: UITableView!
    @IBOutlet weak var search_retailer: UISearchBar!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewTableContainer: UIView!
    @IBOutlet weak var viewMapContainer: UIView!
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var arrRetailers = [Record]()
    var arrSearchResult = [SearchRecord]()
    var arrRetailersCopy = [Record]()
    var isSearch:Bool = false
    var arrCategory = [CategoryData]()
    var selectedCat = ""
    var selectedIndex: Int = 0
    var currentPageNo:Int = 0
    var totalCount:Int = 0
    private let locationManager = CLLocationManager()
    var coordinateBounds = GMSCoordinateBounds()
    let nearByEventsArray = [
        ["id": 1.0, "latitude": 22.7196, "longitude": 75.8577],
        ["id": 2.0, "latitude": 22.6013, "longitude": 75.3025],
        ["id": 3.0, "latitude": 23.1765, "longitude": 75.7885]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }

    
    private func setup() {
        mapView.delegate = self
        self.tblView_search.refreshControl = refresh
        self.addSpinner()
        registerXib()
        setupLocationManager()
//        showUsersLocationOnMap()
        getRetailerCategory()
        viewTableContainer.isHidden = false
        viewMapContainer.isHidden = true
        [btnProfile, btnNotification, btnMap, btnSearch].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        self.currentPageNo = 0
        getRetailersFor(cat: arrCategory[selectedIndex].catID ?? "")
    }
    
    private func loadMore() {
        currentPageNo += 1
        getRetailersFor(cat: arrCategory[selectedIndex].catID ?? "")
    }

    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblView_search.bounds.size.width, height: 44)
        tblView_search.tableFooterView = spinner
        tblView_search.tableFooterView?.isHidden = true
    }
    
    private func navigateToProfile() {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.Menu), animated: true)
    }
    
    private func navigateToNotification() {
//        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.Menu), animated: true)
    }
    
    private func updateContainerViews(showMap:Bool) {
        viewTableContainer.isHidden = showMap
        viewMapContainer.isHidden = !showMap
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        switch sender {
        case btnProfile:
            navigateToProfile()
        case btnNotification:
            self.pushToNotification()
        case btnMap:
            updateContainerViews(showMap: true)
        case btnSearch:
            updateContainerViews(showMap: false)
        default:
            break
        }
    }
    
    private func setupLocationManager() {
        if CLLocationManager.locationServicesEnabled()  {
            switch(CLLocationManager.authorizationStatus())  {
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.delegate = self
                locationManager.requestWhenInUseAuthorization()
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
                mapView.isMyLocationEnabled = true
                mapView.settings.myLocationButton = true
                locationManager.startMonitoringSignificantLocationChanges()
                print("Authorize.")
                break
            case .notDetermined:
                print("Not determined.")
                break
            case .restricted:
                print("Restricted.")
                break
            case .denied:
                print("Denied.")
            default:
                print("default.")
                break
            }
        }
    }
    
    func showUsersLocationOnMap() {
        // To make visible all the location marker on map at the same time
        for data in nearByEventsArray {
            let location = CLLocationCoordinate2D(latitude: data["latitude"]!, longitude: data["longitude"]!)
            print("location: \(location)")
            self.mapView.addLocationMarker(location)
            coordinateBounds = coordinateBounds.includingCoordinate(location)
        }
        //     mapView.camera = GMSCameraPosition(target: lat, zoom: 9, bearing: 0, viewingAngle: 0)
        mapView.animate(with: GMSCameraUpdate.fit(coordinateBounds, withPadding: 60))
    }
    
    private func addLocationMarker(_ location:CLLocationCoordinate2D) { //data: [String: Any]
        let marker = GMSMarker()
        marker.position = location
        marker.icon = GOOGLE_MAP_MARKER
        marker.title = "m@k"
        //                let venueLocation = CLLocation(latitude: location.latitude!, longitude: location.longitude!)
        //                var distance = venueLocation.distance(from: venueLocation)
        marker.tracksInfoWindowChanges = true
        marker.userData = location
        marker.map = self.mapView
        
        //        let latitude = data["latitude"]
        //        let longitude = data["longitude"]
        //        var distanceString = ""
        //        if distance > 1000 {
        //            distance = distance / 1000
        //            distanceString = "\((distance.rounded(toPlaces: 2)).toString) km far from your location"
        //        } else {
        //            distanceString = "\((distance.rounded(toPlaces: 2)).toString) meter far from your location"
        //        }
    }
}

extension SearchVC: UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func registerXib() {
        tblView_search.register(UINib(nibName: Cell.Search, bundle: nil), forCellReuseIdentifier: Cell.Search)
        cllctn_category.register(UINib(nibName: Cell.Category, bundle: nil), forCellWithReuseIdentifier: Cell.Category)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return arrSearchResult.count
        } else {
            return arrRetailers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearch {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.Search) as! SearchCell
            cell.lbl_retailerName.text = arrSearchResult[indexPath.row].busiName
            cell.lbl_distance.text = "\(arrSearchResult[indexPath.row].busiAddress ?? ""),\(arrSearchResult[indexPath.row].city ?? ""),\(arrSearchResult[indexPath.row].country ?? "")"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.Search) as! SearchCell
            cell.lbl_retailerName.text = arrRetailers[indexPath.row].busiName
            cell.lbl_distance.text = "\(arrRetailers[indexPath.row].busiAddress ?? ""),\(arrRetailers[indexPath.row].city ?? ""),\(arrRetailers[indexPath.row].country ?? "")"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = LaQueueStoryBoard.Search(controller: Controller.RetailerDetails) as? RetailerDetailsVC else {return}
        if isSearch {
            vc.busId = arrSearchResult[indexPath.row].busiID ?? ""
            vc.busCode = arrSearchResult[indexPath.row].busiCode ?? ""
        } else {
            vc.busId = arrRetailers[indexPath.row].busiID ?? ""
            vc.busCode = arrRetailers[indexPath.row].busiCode ?? ""
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrRetailers.count < totalCount && indexPath.row == arrRetailers.count - 1 && totalCount > 10 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
            loadMore()
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.Category, for: indexPath) as! CategoryCell
        cell.lbl_catName.text = arrCategory[indexPath.row].catName
        if selectedIndex == indexPath.item {
            cell.lbl_catName.backgroundColor = #colorLiteral(red: 0.007865474559, green: 0.5494334102, blue: 0.5842353106, alpha: 1)
            cell.lbl_catName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.lbl_catName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.lbl_catName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.5, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        getRetailersFor(cat: arrCategory[indexPath.row].catID ?? "")
        selectedCat = arrCategory[indexPath.row].catID ?? ""
        collectionView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let searchText = "\(searchBar.text ?? "")\(text)"
        
        if searchText.count % 2 == 0 {
            print(searchText)
            searchRetailerFor(text: searchText)
        }
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearch = true
        if searchBar.text?.count == 0 {
            isSearch = false
            self.arrRetailers = arrRetailersCopy
            tblView_search.reloadSections(IndexSet(integer: 0), with: .fade)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

extension SearchVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? GMSMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        
        if !arrRetailers.isEmpty {
            
        }
        
        //        selectedLocation = CLLocation(latitude: placeMarker.place.coordinate.latitude, longitude: placeMarker.place.coordinate.longitude)
        //        var distance = currentLocation.distance(from: selectedLocation)
        //        var distanceString = ""
        //        if distance > 1000 {
        //            distance = distance / 1000
        //            distanceString = "\((distance.rounded(toPlaces: 2)).toString) km far from your location"
        //        } else {
        //            distanceString = "\((distance.rounded(toPlaces: 2)).toString) meter far from your location"
        //        }
        
        //        infoView.nameLabel.text = placeMarker.place.name
        //        infoView.distanceLabel.text = distanceString
        //        mapView.camera = GMSCameraPosition(target: selectedLocation.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        //        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}

// MARK: - CLLocationManagerDelegate
extension SearchVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
}

// MARK:- API Call
extension SearchVC {
    func getRetailerCategory() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.categories, method: HTTPMethod.get, Param: nil, header: header , type: CategoryModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrCategory = isResponse.data ?? []
                    self.getRetailersFor(cat: self.arrCategory[0].catID ?? "")
                    self.selectedCat = self.arrCategory[0].catID ?? ""
                    self.cllctn_category.reloadData()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func searchRetailerFor(text:String) {
        var authorization = ""
        var userId = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let uId = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid {
            authorization = auth
            userId = uId
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "cat_id":selectedCat,
            "page_no":String(currentPageNo),
            "search_text":text
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.searchText, method: HTTPMethod.post, Param: param, header: header , type: SearchModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrSearchResult = isResponse.data?[0].record ?? []
                    self.tblView_search.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getRetailersFor(cat:String) {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "category_id":cat,
            "page_no":"0"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.home, method: HTTPMethod.post, Param: param, header: header , type: RetailersModel.self, viewController: self) { (response) in
            self.tblView_search.refreshControl?.endRefreshing()
            if let isResponse = response {
                self.arrRetailers.removeAll()
                if isResponse.status == 1 {
                    self.arrRetailers = isResponse.data?[0].record ?? []
                    self.arrRetailersCopy = self.arrRetailers
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
                Loader.shared.hideIndicator()
                self.tblView_search.reloadData()
            }
        }
    }
}
