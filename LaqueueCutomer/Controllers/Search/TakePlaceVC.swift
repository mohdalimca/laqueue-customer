//
//  TakePlaceVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 11/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class TakePlaceVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txt_people: UITextField!
    @IBOutlet weak var txtView_message: UITextView!
    
    var datacompletion:((String,String) -> ())?
    var capacity:Int!
    let dropdown = DropDown()
    let person = ["1","2","3","4"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_people.delegate = self
        txt_people.keyboardType = .numberPad
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
//        dropdown.anchorView = textField
//        dropdown.dataSource = person
//        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
//        dropdown.direction = .bottom
//        dropdown.selectionAction = { (index, item) in
//            print(index,item)
//            textField.text = item
//        }
//        dropdown.show()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newText = (textField.text as! NSString).replacingCharacters(in: range, with: string)
//        if newText.toInt > capacity {
//            self.func_Alert(message: "Capacity for this queue is \(capacity) you can exceed. Please enter less than or equal to queue ciy.")
//        }
//        return newText.toInt <= capacity    // 10 Limit Value
        return true
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_people.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter number of people"
        }
        
        if txt_people.text!.toInt > capacity {
            msg = "Capacity for this queue is \(capacity!) you can exceed. Please enter less than or equal to queue ciy."
        }
        
//        else if txtView_message.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
//            msg = "Please enter message"
//        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            datacompletion?(txt_people.text ?? "",txtView_message.text ?? "")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
