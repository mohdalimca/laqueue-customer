//
//  RetailerDetailsVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 07/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces

class RetailerDetailsVC: UIViewController {
    
    @IBOutlet weak var lbl_retailerName: UILabel!
    @IBOutlet weak var tblview_queues: UITableView!
    @IBOutlet weak var lbl_descp: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnProfile: UIButton!
    
    var businessDetail: BusinessDetail?
    var busId = ""
    var busCode = ""
    var arrQueueDetail = [QueueDetail]()
    var queueId = ""
    var slotId = ""
    var numOfPeople = ""
    var message = "test message"
    var selectedSlot: SlotInfo!
    private let locationManager = CLLocationManager()
    var coordinateBounds = GMSCoordinateBounds()
    let nearByEventsArray = [
        ["id": 1.0, "latitude": 22.7196, "longitude": 75.8577]
        //           ["id": 2.0, "latitude": 22.6013, "longitude": 75.3025],
        //           ["id": 3.0, "latitude": 23.1765, "longitude": 75.7885]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        btnProfile.setProfileButtonImage()
        mapView.delegate = self
        registerXib()
        setupLocationManager()
        getRetailerDetails()
//        showUsersLocationOnMap()
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    private func showUsersLocationOnMap() {
        for data in nearByEventsArray {
            let location = CLLocationCoordinate2D(latitude: data["latitude"]!, longitude: data["longitude"]!)
            print("location: \(location)")
            self.mapView.addLocationMarker(location)
            coordinateBounds = coordinateBounds.includingCoordinate(location)
            //            mapView.camera = GMSCameraPosition(target: location, zoom: 1)
        }
        mapView.animate(with: GMSCameraUpdate.fit(coordinateBounds, withPadding: 60))
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        self.pushToNotification()
    }
}

extension RetailerDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func registerXib() {
        tblview_queues.register(UINib(nibName: Cell.Queue, bundle: nil), forCellReuseIdentifier: Cell.Queue)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQueueDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.Queue) as! QueueCell
        cell.lbl_queuename.text = arrQueueDetail[indexPath.row].queName
        let capacity = arrQueueDetail[indexPath.row].capacity ?? "-"
        cell.lblCapacity.text = "Capacity: \(capacity)"
        let people = (arrQueueDetail[indexPath.row].totalQueueNo?.toInt ?? 0)
        if people > 0 {
            cell.lbl_persons.text = "\(people) person ahead."
        } else {
            cell.lbl_persons.text = "No person ahead."
        }
        
        cell.lbl_queueDetail.text = arrQueueDetail[indexPath.row].queDescription
        if arrQueueDetail[indexPath.row].status  == "Play" {
            cell.lblQueueStatus.text = "Open"
            cell.lblQueueStatus.textColor = UIColor(hex: "#00B050")
        } else {
            cell.lblQueueStatus.text = "Close"
            cell.lblQueueStatus.textColor = UIColor(hex: "#FF0000")
        }
        
        let slot = arrQueueDetail[indexPath.row].slotInfo?.filter {$0.isCustom != "" }
//        var slotText = "\(slot?.first?.queOneTo ?? "") to \(slot?.first?.queOneFrom ?? "")"
//        if slot?.first?.queTwoTo != nil && slot?.first?.queTwoTo != "" {
//            slotText = slotText + " and \(slot?.first?.queTwoTo ?? "") to \(slot?.first?.queTwoFrom ?? "")"
//        }
        cell.lblSlot.text = "\(slot?.first?.queOneTo ?? "") to \(slot?.first?.queOneFrom ?? "")"
//        cell.lblSlot.text = "\(slot?.first?.queOneTo ?? "") to \(slot?.first?.queOneFrom ?? "") and \(slot?.first?.queTwoTo ?? "") to \(slot?.first?.queTwoFrom ?? "")"
        cell.btnTakePlace = {
            if self.arrQueueDetail[indexPath.row].status  != "Play" {
                self.func_Alert(message: "Queue is not open now. Wait and try later to take a place in queue.\nThank you")
                return
            }
            guard let vc = LaQueueStoryBoard.Search(controller: Controller.TakePlace) as? TakePlaceVC else {return}
            vc.capacity = self.arrQueueDetail[indexPath.row].capacity?.toInt ?? 0
            vc.modalPresentationStyle = .overCurrentContext
            vc.datacompletion = { people, msg in
                self.numOfPeople = people
                self.message = msg
                self.takePlace()
            }
            
            self.queueId = self.arrQueueDetail[indexPath.row].queID ?? ""
            self.selectedSlot = (self.arrQueueDetail[indexPath.row]).slotInfo?.first
            self.present(vc, animated: true, completion: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - CLLocationManagerDelegate
extension RetailerDetailsVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
}

extension RetailerDetailsVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? GMSMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        //        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}

extension RetailerDetailsVC {
    func getRetailerDetails() {
        var authorization = ""
        let currentDate = Date().timeIntervalSince1970
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "busi_id":busId,
            "current_day":"0",
            "current_date":"\(Int(currentDate))"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.retailerDetail, method: HTTPMethod.post, Param: param, header: header , type: RetailerDetailModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.businessDetail = isResponse.data?.businessDetail
                    self.lbl_descp.text = isResponse.data?.businessDetail?.busiDescription
                    self.lbl_retailerName.text = isResponse.data?.businessDetail?.busiName?.capitalized
                    let arr = isResponse.data?.businessDetail?.queueDetails
//                    self.arrQueueDetail = isResponse.data?.businessDetail?.queueDetails ?? []
                    self.arrQueueDetail = arr?.first ?? []
                    self.tblview_queues.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func takePlace() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var day = ""
        switch date.dayOfWeek() {
        case "Monday":
            day = "1"
        case "Tuesday":
            day = "2"
        case "Wednesday":
            day = "3"
        case "Thursday":
            day = "4"
        case "Friday":
            day = "5"
        case "Saturday":
            day = "6"
        default:
            day = "0"
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        
        let param : [String:Any] = [
            "busi_id":self.businessDetail?.busiID ?? "",
            "busi_code":self.businessDetail?.busiCode ?? "",
            "day":day,
            "que_id":queueId,
            "slot_id":selectedSlot.slotId ?? "0",
            "slot_time":"0",
            "people":numOfPeople,
            "booking_date":String(UInt64(Date().timeIntervalSince1970)),
            "message":message
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.takePlace, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
}
