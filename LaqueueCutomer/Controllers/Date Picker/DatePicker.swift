//
//  DatePicker.swift
//  Clustry
//
//  Created by cis on 05/12/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

protocol GetDateDelagate {
    func getDate(_ date:String, timestamp:TimeInterval)
}

enum DatePickerDay:Int {
    case today = 1
    case tomorrow = 2
    case none = 0
}

class DatePicker: UIViewController {
    
    //    var pickerType : PickerType?
    @IBOutlet weak var datePicker: UIDatePicker!
    var delegate:GetDateDelagate?
    var index = 0
    var selectedValue = ""
    var dateFormatStr = "dd-MMM-yyyy"
    var setMinDate: Bool?
    var setMaxDate: Bool?
    var mode: UIDatePicker.Mode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        if setMinDate != nil {
            self.datePicker.minimumDate = Date()
        }
        if setMaxDate != nil {
            self.datePicker.maximumDate = Date()
        }
        self.datePicker.datePickerMode = mode
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func action_Cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func action_Done(_ sender: UIBarButtonItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormatStr
        self.selectedValue = formatter.string(from: datePicker.date)
        self.delegate?.getDate(self.selectedValue,  timestamp:self.datePicker.date.timeIntervalSince1970)
        self.dismiss(animated: true, completion: nil)
    }
}
