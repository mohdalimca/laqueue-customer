//
//  PlaceRequestVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire

class PlaceRequestVC: UIViewController {
    
    @IBOutlet weak var view_map: GMSMapView!
    @IBOutlet weak var lbl_timer: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_queueStatus: UILabel!
    @IBOutlet weak var lbl_dateTime: UILabel!
    @IBOutlet weak var lbl_wait: UILabel!
    @IBOutlet weak var lbl_person: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_restMessage: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnPlaceStatus: UIButton!
    @IBOutlet weak var imageRequestStatus: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    
    private let locationManager = CLLocationManager()
    var coordinateBounds = GMSCoordinateBounds()
    let nearByEventsArray = [
        ["id": 1.0, "latitude": 22.7196, "longitude": 75.8577]
        //           ["id": 2.0, "latitude": 22.6013, "longitude": 75.3025],
        //           ["id": 3.0, "latitude": 23.1765, "longitude": 75.7885]
    ]
    
    let cancelImage = UIImage(named: "queue_cancel")
    let acceptImage = UIImage(named: "queue_joined")
    var bookingId = ""
    var timeSeconds = 0
    var timer:Timer?
    var bookingStatus: BookingStatus!
    var bookinStatusMessage: String = ""
    var refreshQueueList: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        btnProfile.setProfileButtonImage()
    }

    private func setup() {
        [btnCancel, btnPlaceStatus].forEach {
            $0?.isHidden = true
        }
        view_map.delegate = self
        setupLocationManager()
        getPlaceData()
    }
    
    fileprivate func viewUpdateForNew( _ isResponse: QueuePlaceModel) {
        btnPlaceStatus.isHidden = true
        btnCancel.isHidden = false
        let message = "Your place in queue : \(isResponse.data?.businessName ?? "--") - \(isResponse.data?.queName ?? "--") will be soon acknowledged."
        lbl_restMessage.text = message
    }
    
    fileprivate func viewUpdateForAcknowledge( _ isResponse: QueuePlaceModel) {
        btnPlaceStatus.isHidden = true
        btnCancel.isHidden = false
        timeSeconds = (isResponse.data?.waitingTime ?? "0").toInt
        timeSeconds = timeSeconds * 60
        let fromDate = (isResponse.data?.lastStatusUpdate ?? "0").toDouble.dateFromTimestamp()
        print("from date \(fromDate)")
        let toDate = (Date().timeIntervalSince1970).dateFromTimestamp()
        print("to date \(toDate)")
        let difference = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
        print("seconds difference \(difference)")
        if (difference.second ?? 0) > 0 {
            timeSeconds = timeSeconds - difference.second!
        }
        self.runTimer()
        btnCancel.isHidden = false
        btnPlaceStatus.isHidden = false
        btnPlaceStatus.titleLabel?.text = "I want an update"
        var message = ""
        if bookingStatus == .some(.Acknowledge) {
            lblStatus.textColor = #colorLiteral(red: 1, green: 0.7529411765, blue: 0, alpha: 1)
            message = "Your place in queue : \(isResponse.data?.businessName ?? "--") - \(isResponse.data?.queName ?? "--") is acknowledged."
        } else {
            lblStatus.textColor = #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
            message = "Your asked : \(isResponse.data?.businessName ?? "--") - \(isResponse.data?.queName ?? "--") for an update 2 times. Limited to 3 updates."
        }
        lbl_restMessage.text = message
    }
    
    fileprivate func viewUpdateForCall( _ isResponse: QueuePlaceModel) {
        timeSeconds = (isResponse.data?.callTimer ?? "0").toInt
        timeSeconds = timeSeconds * 60
        let fromDate = (isResponse.data?.lastStatusUpdate ?? "0").toDouble.dateFromTimestamp()
        print("from date \(fromDate)")
        let toDate = (Date().timeIntervalSince1970).dateFromTimestamp()
        print("to date \(toDate)")
        let difference = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
        print("seconds difference \(difference)")
        if (difference.second ?? 0) > 0 {
            timeSeconds = timeSeconds - difference.second!
        }
        self.runTimer()
        btnPlaceStatus.isHidden = true
        btnCancel.isHidden = true
        let message = " \(isResponse.data?.businessName ?? "--") - \(isResponse.data?.queName ?? "--") called you. They can signal you as NoShow."
        lbl_restMessage.text = message
    }
    
    fileprivate func viewUpdateForCancelNoshow() {
        self.lbl_timer.isHidden = true
        btnPlaceStatus.isHidden = false
        btnCancel.isHidden = true
        btnPlaceStatus.isHidden = true
        lbl_restMessage.text = ""
    }
    
    fileprivate func showData(_ isResponse: QueuePlaceModel) {
        self.lbl_queueStatus.text = "Queue Joined"
        self.lbl_dateTime.text = isResponse.data?.bookingTime?.toDouble.placeDetailDate
        self.lbl_address.text = "\(isResponse.data?.firstname ?? "") \(isResponse.data?.lastname ?? "")"
        self.lbl_person.text = "\(isResponse.data?.totalQueueNo ?? "") person ahead you"
        self.lbl_message.text = isResponse.data?.message ?? ""

        lbl_timer.isHidden = false
        imageRequestStatus.isHidden = true
        bookingStatus = BookingStatus(rawValue: isResponse.data?.status ?? "")
        lblStatus.text = bookingStatus.rawValue
        let watingTime = Date().timePassed(from: isResponse.data?.bookingTime?.toDouble.dateFromTimestamp() ?? Date())
        self.lbl_wait.text = "waited \(watingTime)"
        switch bookingStatus {
        case .New:
            bookinStatusMessage = "Your request us cancelled."
            lbl_timer.isHidden = true
            imageRequestStatus.isHidden = false
            imageRequestStatus.image = acceptImage!
            viewUpdateForNew(isResponse)
            lblStatus.textColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)

        case .Acknowledge, .Update:
            bookinStatusMessage = "Update to retailer requested. Retailer will get back to you soon."
            viewUpdateForAcknowledge(isResponse)
        case .Call:
            lblStatus.textColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            viewUpdateForCall( isResponse)
        case .Accept:
            lblStatus.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            let watingTime = isResponse.data?.lastStatusUpdate?.toDouble.dateFromTimestamp().timePassed(from: isResponse.data?.bookingTime?.toDouble.dateFromTimestamp() ?? Date())
            self.lbl_wait.text = "waited \(watingTime ?? "0 min")"
            lbl_timer.isHidden = true
            imageRequestStatus.image = acceptImage!
        case .Cancel, .Noshow:
            lblStatus.textColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            lbl_timer.isHidden = true
            imageRequestStatus.isHidden = false
            imageRequestStatus.image = cancelImage!
            viewUpdateForCancelNoshow()
            let watingTime = isResponse.data?.lastStatusUpdate?.toDouble.dateFromTimestamp().timePassed(from: isResponse.data?.bookingTime?.toDouble.dateFromTimestamp() ?? Date())
            self.lbl_wait.text = "waited \(watingTime ?? "0 min")"
        default:  break
        }
    }
    
    private func checkForMainContainerAndPopVC() {
        let vc = (LaQueueStoryBoard.Queue(controller: Controller.QueueVC) as? QueueVC)!
        if !(self.navigationController!.viewControllers.contains(vc)) {
            var vcs = navigationController?.viewControllers
            vcs![0] = vc
            navigationController?.viewControllers = vcs!
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    
    // MARK:- Button Action
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.checkForMainContainerAndPopVC()
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        self.pushToNotification()
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.func_AlertWithTwoOption(message: "Do you want to cancel and proceed?", optionOne: "Yes", optionTwo: "No", actionOne: {
            self.bookingStatus = .Cancel
            self.updateBookingStatus()
        }) {
            print("no tapped")
        }
        
    }
    
    @IBAction func btnPlaceStatusTap(_ sender: UIButton) {
        if bookingStatus == .some(.Acknowledge) {
            bookingStatus = .Update
        }
        updateBookingStatus()
    }
}

// MARK:- Instance Methods
extension PlaceRequestVC {
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        timeSeconds -= 1     //This will decrement(count down)the timeSeconds.
        var strTime = ""
        let hours = timeSeconds / 3600
        let minutes = timeSeconds / 60 % 60
        let seconds = timeSeconds % 60
        if hours > 0 {
            strTime = String(format:" %02i:%02i:%02i hr", hours, minutes, seconds)
        } else {
            strTime = String(format:" %02i:%02i min", minutes, seconds)
        }
        lbl_timer.text = strTime //This will update the label.
        if timeSeconds <= 0 {
            lbl_timer.text = "00 : 00 min"
            timer?.invalidate()
            timer = nil
            timeSeconds = 30
        }
    }
}


extension PlaceRequestVC: GMSMapViewDelegate, CLLocationManagerDelegate {
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    private func showUsersLocationOnMap() {
        for data in nearByEventsArray {
            let location = CLLocationCoordinate2D(latitude: data["latitude"]!, longitude: data["longitude"]!)
            print("location: \(location)")
            self.view_map.addLocationMarker(location)
            coordinateBounds = coordinateBounds.includingCoordinate(location)
            //            mapView.camera = GMSCameraPosition(target: location, zoom: 1)
        }
        view_map.animate(with: GMSCameraUpdate.fit(coordinateBounds, withPadding: 60))
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        view_map.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
    
}

// MARK- API Calls
extension PlaceRequestVC {
    func updateBookingStatus() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "booking_id":bookingId,
            "status":bookingStatus.rawValue,
            "last_status_update": String(UInt64(Date().timeIntervalSince1970))
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.bookingStatus, method: HTTPMethod.post, Param: param, header: header , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                self.refreshQueueList?()
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getPlaceData() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "booking_id":bookingId
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.placeInQueue, method: HTTPMethod.post, Param: param, header: header , type: QueuePlaceModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.showData(isResponse)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
}
