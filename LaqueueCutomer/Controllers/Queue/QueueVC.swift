//
//  QueueVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class QueueVC: UIViewController {
    
    @IBOutlet weak var lblFilter: UILabel!
    @IBOutlet weak var lblSavedTime: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var tblView_queue: UITableView!
    @IBOutlet weak var viewNoData: UIView! {
        didSet {
            viewNoData.isHidden = true
        }
    }
    
    var refreshOption: RefreshOption = .top
    var isFromSideMenu: Bool = false
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    let arrBookingStatus : [BookingStatus] = [
        BookingStatus.New,
        BookingStatus.Acknowledge,
        BookingStatus.Noshow,
        BookingStatus.Call,
        BookingStatus.Update,
        BookingStatus.Cancel,
        BookingStatus.All
    ]
    
    var arrQueues = [QueueRecord]()
    var currentPageNo:Int = 0
    var totalCount:Int = 0
    var filterBookingStatus: BookingStatus = .All
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        btnProfile.setProfileButtonImage()
        self.getQueueList()
    }
    
    private func setup() {
        self.registerXib()
        self.tblView_queue.refreshControl = refresh
        self.addSpinner()
        [btnFilter, btnProfile, btnNotification].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        if !isFromSideMenu {
            lblFilter.text = "All"
            btnFilter.isHidden = true
            self.lblSavedTime.text = "--"
        }
        self.getQueueList()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblView_queue.bounds.size.width, height: 44)
        tblView_queue.tableFooterView = spinner
        tblView_queue.tableFooterView?.isHidden = true
    }
    
    private func refreshQueueList() {
        refreshOption = .top
        self.currentPageNo = 0
        self.getQueueList()
    }
    
    private func showPicker() {
        guard let picker = LaQueueStoryBoard.Queue(controller: Controller.Picker) as? PickerViewController else {return}
        picker.delegate = self
        picker.data = arrBookingStatus.map { $0.rawValue}
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overFullScreen
        self.present(picker, animated: true, completion: nil)
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        switch sender {
        case btnFilter: showPicker()
        case btnProfile:
            self.openMenu()
        case btnNotification:
            self.pushToNotification()
        default: break
        }
    }
}

extension QueueVC: UITableViewDataSource, UITableViewDelegate {
    
    func registerXib() {
        tblView_queue.register(UINib(nibName: Cell.QueueList, bundle: nil), forCellReuseIdentifier: Cell.QueueList)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQueues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.QueueList) as! QueueListCell
        cell.configure(arrQueues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nc = LaQueueStoryBoard.Queue(controller: Controller.PlaceRequest) as? PlaceRequestVC else {return}
        nc.refreshQueueList = refreshQueueList
        nc.bookingId = arrQueues[indexPath.row].bookingID ?? ""
        self.navigationController?.pushViewController(nc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrQueues.count < totalCount && indexPath.row == arrQueues.count - 1 && totalCount > 20 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
            loadMore()
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        refreshQueueList()
    }
    
    private func loadMore() {
        refreshOption = .bottom
        currentPageNo += 1
        self.getQueueList()
    }
}

extension QueueVC: GetPickerValueDelagate {
    func getPickerValue(_ value: String, _ index: Int) {
        if index != -1 {
            filterBookingStatus = arrBookingStatus[index]
            lblFilter.text = filterBookingStatus.rawValue
            getQueueList()
        }
    }
}


// MARK:- API Calls
extension QueueVC {
    func getQueueList() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        var date = ""
        var filter = filterBookingStatus.rawValue
        if !self.isFromSideMenu {
            date = Date().myOrderDate
        }
        
        if filterBookingStatus == .All {
            filter = ""
        }
        
        let param = [
            "page_no" : String(currentPageNo),
//            "current_date": "2020-12-03",
            "current_date": date,
            "filter": filter
        ]
        
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.queueList, method: HTTPMethod.post, Param: param, header: header , type: QueueListModel.self, viewController: self) { (response) in
            self.tblView_queue.refreshControl?.endRefreshing()
            if let isResponse = response {
                Loader.shared.hideIndicator()
                if isResponse.status == 1 {
                    self.onSuccess(isResponse)
                } else {
                    self.onNodata()
                }
            }
        }
    }
    
    private func onSuccess(_ isResponse:QueueListModel) {
        self.lblSavedTime.text = "--"
        self.btnFilter.isHidden = false
        self.lblFilter.isHidden = false
        self.self.tblView_queue.isHidden = false
        self.viewNoData.isHidden = true
        self.totalCount = isResponse.data?[0].totalcount?.toInt ?? self.totalCount
        if refreshOption == .top {
            self.arrQueues.removeAll()
        }
        self.arrQueues.append(contentsOf: isResponse.data?[0].record ?? [])
        self.tblView_queue.reloadSections(IndexSet(integer: 0), with: .fade)
    }
    
    private func onNodata() {
        self.lblSavedTime.text = "--"
        self.btnFilter.isHidden = false
        self.lblFilter.isHidden = false
        self.self.tblView_queue.isHidden = true
        self.viewNoData.isHidden = false
    }
}
