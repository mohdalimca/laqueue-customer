//
//  NewHomeVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class NewHomeVC: UIViewController {
    
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var tblview_home: UITableView!
    @IBOutlet weak var cllction_home: UICollectionView!
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var arrRetailers = [Record]()
    var arrCategory = [CategoryData]()
    var selectedIndex: Int = 0
    var currentPageNo:Int = 0
    var totalCount:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btnProfile.setProfileButtonImage()
    }

    
    private func setup() {
        registerXib()
        self.tblview_home.refreshControl = refresh
        self.addSpinner()
        getRetailerCategory()
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        self.currentPageNo = 0
        getRetailersFor(cat: arrCategory[selectedIndex].catID ?? "")
    }
    
    private func loadMore() {
        currentPageNo += 1
        getRetailersFor(cat: arrCategory[selectedIndex].catID ?? "")
    }

    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblview_home.bounds.size.width, height: 44)
        tblview_home.tableFooterView = spinner
        tblview_home.tableFooterView?.isHidden = true
    }

    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.Menu), animated: true)
    }
    
    @IBAction func btn_notifications(_ sender: UIButton) {
        
    }    
}

extension NewHomeVC: UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func registerXib() {
        cllction_home.register(UINib(nibName: Cell.Category, bundle: nil), forCellWithReuseIdentifier: Cell.Category)
        tblview_home.register(UINib(nibName: Cell.Restaurant, bundle: nil), forCellReuseIdentifier: Cell.Restaurant)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRetailers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.Restaurant) as! RestaurantCell
        cell.lbl_restName.text = arrRetailers[indexPath.row].busiName ?? ""
        cell.lbl_location.text = "\(arrRetailers[indexPath.row].busiAddress ?? ""),\(arrRetailers[indexPath.row].city ?? ""),\(arrRetailers[indexPath.row].country ?? "")"
        cell.img_restaurant.kf.indicatorType = .activity
        cell.img_restaurant.kf.setImage(with: URL(string: arrRetailers[indexPath.row].busiImg ?? ""), options: [.transition(.fade(0.5))])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nc = LaQueueStoryBoard.Search(controller: Controller.RetailerDetails) as? RetailerDetailsVC else {return}
        nc.busId = arrRetailers[indexPath.row].busiID ?? ""
        self.navigationController?.pushViewController(nc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrRetailers.count < totalCount && indexPath.row == arrRetailers.count - 1 && totalCount > 10 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
            loadMore()
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.Category, for: indexPath) as! CategoryCell
        cell.lbl_catName.text = arrCategory[indexPath.row].catName
        if selectedIndex == indexPath.item {
            cell.lbl_catName.backgroundColor = #colorLiteral(red: 0.007865474559, green: 0.5494334102, blue: 0.5842353106, alpha: 1)
            cell.lbl_catName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.lbl_catName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.lbl_catName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.5, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        getRetailersFor(cat: arrCategory[indexPath.item].catID ?? "")
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCell {
        //            cell.lbl_catName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //            cell.lbl_catName.textColor = #colorLiteral(red: 0.007865474559, green: 0.5494334102, blue: 0.5842353106, alpha: 1)
        //        }
    }
}


// API Calls
extension NewHomeVC {
    private func getRetailerCategory() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.categories, method: HTTPMethod.get, Param: nil, header: header , type: CategoryModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrCategory = isResponse.data ?? []
                    self.getRetailersFor(cat: self.arrCategory[0].catID ?? "")
                    self.cllction_home.reloadData()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getRetailersFor(cat:String) {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "category_id":cat,
            "page_no":String(currentPageNo)
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.home, method: HTTPMethod.post, Param: param, header: header , type: RetailersModel.self, viewController: self) { (response) in
            if let isResponse = response {
                self.tblview_home.refreshControl?.endRefreshing()
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.totalCount = isResponse.data?[0].totalcount ?? self.totalCount
                    self.arrRetailers = isResponse.data?[0].record ?? []
                    self.tblview_home.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.arrRetailers.removeAll()
                    self.tblview_home.reloadSections(IndexSet(integer: 0), with: .fade)
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}

