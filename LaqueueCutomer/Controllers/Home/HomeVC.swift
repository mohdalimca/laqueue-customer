//
//  HomeVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 20/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var cllction_restaurant: UICollectionView!
    @IBOutlet weak var cllction_adminstration: UICollectionView!
    @IBOutlet weak var cllction_others: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerXib()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btn_profile.setProfileButtonImage()
    }

    @IBAction func btn_profileTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_seeAllTap(_ sender: UIButton) {
    }
    
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func registerXib() {
        cllction_others.register(UINib(nibName: Cell.Home, bundle: nil), forCellWithReuseIdentifier: Cell.Home)
        cllction_restaurant.register(UINib(nibName: Cell.Home, bundle: nil), forCellWithReuseIdentifier: Cell.Home)
        cllction_adminstration.register(UINib(nibName: Cell.Home, bundle: nil), forCellWithReuseIdentifier: Cell.Home)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.Home, for: indexPath) as! HomeCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 1.5, height: collectionView.frame.height)
    }
}
