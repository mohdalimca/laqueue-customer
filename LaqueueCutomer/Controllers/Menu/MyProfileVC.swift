//
//  MyProfileVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 29/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class MyProfileVC: UIViewController {

    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_password: UILabel!
    @IBOutlet weak var lbl_lang: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    
    var email = ""
    var profilePic = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getProfileDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func didUpdateProfile() {
        self.getProfileDetails()
    }
    
    private func populateDetails(_ isResponse:ProfileModel) {
        self.profilePic = isResponse.data?.profile?.userPic ?? ""
        UserStore.save(userPic:profilePic)
        btnProfile.setProfileButtonImage()
        self.img_profile.kf.indicatorType = .activity
        self.img_profile.kf.setImage(with: URL(string: isResponse.data?.profile?.userPic ?? ""), options: [.transition(.fade(0.2))])
        self.lbl_name.text = "\(isResponse.data?.profile?.firstname ?? "") \(isResponse.data?.profile?.lastname ?? "")"
        self.lbl_email.text = isResponse.data?.profile?.userEmail ?? ""
        self.lbl_phone.text =  "+\(isResponse.data?.profile?.countryCode ?? "")" + " - " + "\(isResponse.data?.profile?.userPhone ?? "")"
        self.lbl_password.text = "********"
        self.email = isResponse.data?.profile?.userEmail ?? ""
    }
    
    // MARK:- Button Action
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.popViewController()
    }
  
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        self.pushToNotification()
    }
    
    @IBAction func btn_editTap(_ sender: UIButton) {
        guard let vc = LaQueueStoryBoard.Menu(controller: Controller.UpdateProfile) as? UpdateProfileVC  else{return}
        vc.email = email
        vc.profilePic = profilePic
        vc.didUpdateProfile = didUpdateProfile
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyProfileVC {
    func getProfileDetails() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.userProfile, method: HTTPMethod.get, Param: nil, header: header , type: ProfileModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.populateDetails(isResponse)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
