//
//  UpdateProfileVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 02/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class UpdateProfileVC: UIViewController {
    
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var txt_fName: UITextField!
    @IBOutlet weak var txt_lName: UITextField!
    @IBOutlet weak var txt_phoneNumber: UITextField!
    @IBOutlet weak var txt_dob: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    //    @IBOutlet weak var txt_dialCode: UITextField!
    @IBOutlet weak var btn_countryCode: UIButton!
    @IBOutlet weak var btn_langFrench: UIButton!
    @IBOutlet weak var btn_langEnglish: UIButton!
    @IBOutlet weak var btn_Back: UIButton!

    
    var imagePicker: ImagePicker!
    var fromLogin = false
    var email = ""
    var profilePic = ""
    var jsonArray: [Any] = []
    var arrCountry: [String] = []
    var countryList = CountryList()
    var selectedCountry: Country!
    var picker: DatePicker! = nil
    var didUpdateProfile: (() -> Void)?
    var showPicker: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setup() {
        if fromLogin {
            btn_Back.isHidden = true
            lbl_email.text = email
            img_profile.kf.setImage(with: URL(string: profilePic), options: [.transition(.fade(0.5))])
        } else {
            lbl_email.text = email
            img_profile.kf.setImage(with: URL(string: profilePic), options: [.transition(.fade(0.5))])
        }
        [btn_countryCode].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        
        [txt_fName, txt_lName, txt_phoneNumber, txt_dob, txt_password].forEach { field in
            field?.delegate = self
        }
        
        picker = LaQueueStoryBoard.Main(controller: Controller.DatePicker) as? DatePicker
        picker.delegate = self
        selectedCountry = Country(countryCode: "FR", phoneExtension: "33")
        btn_countryCode.setTitle(countryCodeText(), for: .normal)
        countryList.delegate = self
        loadCountryJsonList()
        getProfileDetails()
    }
    
    private func loadCountryJsonList() {
        guard let path = Bundle.main.path(forResource: "country_list", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]  {
                print(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        present(countryList, animated: true, completion: nil)
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_langTap(_ sender: UIButton) {
        if sender == btn_langFrench {
            btn_langFrench.isSelected = true
            btn_langEnglish.isSelected = false
        } else {
            btn_langFrench.isSelected = false
            btn_langEnglish.isSelected = true
        }
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            updateUserProfile()
        }
    }
    
    private func countryCodeText() -> String {
        var strCountry = ""
        strCountry.append(selectedCountry.flag! + " ")
        strCountry.append(selectedCountry.countryCode + " +")
        strCountry.append(selectedCountry.phoneExtension)
        return strCountry
    }
    
    private func showDatePicker() {
        picker.selectedValue = ""
        picker.mode = .date
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overFullScreen
        self.present(picker, animated: true, completion: nil)
    }
}

extension UpdateProfileVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_dob {
            self.view.endEditing(true)
            if showPicker == nil || showPicker == true {
                showPicker = false
                self.showDatePicker()
            }
        }
    }
}


extension UpdateProfileVC: GetDateDelagate {
    func getDate(_ date: String, timestamp: TimeInterval) {
        txt_dob.text = date
        showPicker = true
        self.view.endEditing(true)
    }
}


extension UpdateProfileVC : CountryListDelegate {
    func selectedCountry(country: Country) {
        selectedCountry = country
        btn_countryCode.setTitle(countryCodeText(), for: .normal)
    }
}

extension UpdateProfileVC : ImagePickerDelegate{
    
    func didSelect(image: UIImage?) {
        guard let img = image else { return }
        self.img_profile.image = img
    }
    
    func validateInputs() -> Bool {
        self.view.endEditing(true)
        self.picker.dismiss(animated: false, completion: nil)
        var msg = ""
        if txt_fName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter first name"
        } else if txt_lName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter last name"
        } else if txt_phoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter phone number"
        } else if btn_countryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter dial code"
        } else if txt_dob.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter date of birth"
        } else if img_profile.image == nil {
            msg = "Please select image"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func getProfileDetails() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.userProfile, method: HTTPMethod.get, Param: nil, header: header , type: ProfileModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.img_profile.kf.indicatorType = .activity
                    self.img_profile.kf.setImage(with: URL(string: isResponse.data?.profile?.userPic ?? ""), options: [.transition(.fade(0.2))])
                    
                    //                    self.img_profile.kf.setImage(with: URL(string: isResponse.data?.profile?.userPic ?? ""))
                    self.lbl_name.text = "\(isResponse.data?.profile?.firstname ?? "") \(isResponse.data?.profile?.lastname ?? "")"
                    self.txt_fName.text = isResponse.data?.profile?.firstname ?? ""
                    self.txt_lName.text = isResponse.data?.profile?.lastname ?? ""
                    self.lbl_email.text = isResponse.data?.profile?.userEmail ?? ""
                    self.txt_phoneNumber.text = isResponse.data?.profile?.userPhone ?? ""
                    if isResponse.data?.profile?.countryCode != "" {
                        self.selectedCountry.phoneExtension = "\(isResponse.data?.profile?.countryCode ?? "")"
                    }
                    self.btn_countryCode.setTitle(self.countryCodeText(), for: .normal)
                    //                        self.btn_countryCode.setTitle("+\(self.selectedCountry.phoneExtension)", for: .normal)
                    self.txt_dob.text = isResponse.data?.profile?.dob ?? ""
                    //                        self.txt_dob.text = isResponse.data?.profile?. ?? ""
                    //                        self.txt_password.text = "********"
                    self.email = isResponse.data?.profile?.userEmail ?? ""
                    self.profilePic = isResponse.data?.profile?.userPic ?? ""
                    
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func updateUserProfile() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "firstname":txt_fName.text ?? "",
            "lastname":txt_lName.text ?? "",
            "country_code":selectedCountry.phoneExtension ,
            "user_phone":txt_phoneNumber.text ?? "",
            "dob": txt_dob.text ?? "",
            "type":"0",
            "business_name": "",
            "busi_description":"",
            "address": "",
            "lat":"",
            "lng":"",
            "country":"",
            "city":"",
            "waiting_time":""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(isMultipart: true, urlString: WebUrl.updateProfile, method: .post, Param: param, header: header, file: ["profile_image" : img_profile.image!], type: NewPasswordModel.self, viewController: self, fullAccess: true, encoding: K3encoding.JSON) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    if self.fromLogin {
                        self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                            UserDefaults.standard.set(1, forKey: "profileStatus")
                            self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.TabBar), animated: true)
                        }
                    } else {
                        self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                            self.didUpdateProfile?()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}

