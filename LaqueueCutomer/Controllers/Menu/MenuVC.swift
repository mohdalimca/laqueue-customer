//
//  MenuVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MenuVC: UIViewController {

    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        self.pushToNotification()
    }
    
    @IBAction func btn_menuOptTap(_ sender: UIButton) {
        self.popViewController()
    }
    
    @IBAction func btn_detailsTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.MyProfile), animated: true)
    }
    
    @IBAction func btn_queueTap(_ sender: UIButton) {
        let queueVC = LaQueueStoryBoard.Queue(controller: Controller.QueueVC) as! QueueVC
        queueVC.isFromSideMenu = true
        self.navigationController?.pushViewController(queueVC, animated: true)
//        self.navigationController?.pushViewController(LaQueueStoryBoard.Queue(controller: Controller.QueueVC), animated: true)
    }
    
    @IBAction func btn_prefrenceTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.Prefrences), animated: true)
    }
    
    
    @IBAction func btn_passwordTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.ChangePassword), animated: true)
    }
    
    
    @IBAction func btn_changePassword(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Menu(controller: Controller.ChangePassword), animated: true)
    }
    
    @IBAction func btn_logoutTap(_ sender: UIButton) {
        logoutUser()
    }
}

extension MenuVC {
    func logoutUser() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.logout, method: HTTPMethod.get, Param: nil, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        LoginSession.shared.updateLoginStatus(false)
                        LoginSession.shared.clearUserDefault()
                        if let wndw = UIApplication.shared.windows.first {
                            if let isNc = LaQueueStoryBoard.Authenticate(controller: "RootController") as? UINavigationController {
                                let oldState = UIView.areAnimationsEnabled
                                UIView.setAnimationsEnabled(false)
                                UIView.transition(with: wndw, duration: 0.5, options: [.transitionCrossDissolve], animations: {
                                    wndw.rootViewController = isNc
                                }, completion: { (isDone) in
                                    UIView.setAnimationsEnabled(oldState)
                                })
                            }
                        }
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
