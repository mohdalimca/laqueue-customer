//
//  SignUpVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 24/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class SignUpVC: UIViewController {

    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    var isCheckedCount = 0
    var isFromSplash = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- Button Actions
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_checkTap(_ sender: UIButton) {
        isCheckedCount += 1
        if isCheckedCount%2 == 0 {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
    }
    
    @IBAction func btn_tncTap(_ sender: UIButton) {
        guard let vc = LaQueueStoryBoard.Authenticate(controller: Controller.StaticContent) as? StaticContentVC else {return}
        vc.urlToLoad = WebUrl.terms
        vc.headerTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_privacyTap(_ sender: UIButton) {
        guard let vc = LaQueueStoryBoard.Authenticate(controller: Controller.StaticContent) as? StaticContentVC else {return}
        vc.urlToLoad = WebUrl.privacy
        vc.headerTitle = "Privacy Policy"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_signinTap(_ sender: UIButton) {
        if validateInputs() {
            registerUsers()
        }
    }
    
    @IBAction func btn_loginTap(_ sender: UIButton) {
        if isFromSplash {
            let vc = LaQueueStoryBoard.Authenticate(controller: "LoginVC") as! LoginVC
            vc.isFromSplash = !isFromSplash
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension SignUpVC {
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_email.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter an email address"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter a valid email address"
        } else if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your password"
        } else if isCheckedCount%2 == 0 {
            msg = "Please check Terms & Conditions and Privacy Policy"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func registerUsers() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "email": txt_email.text ?? "",
            "password": txt_password.text ?? "",
            "type": "0"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.registration, method: HTTPMethod.post, Param: param, header: header , type: SignUpModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: "\(isResponse.message ?? "") OTP is \(isResponse.data?.register?.otp ?? "")", optionOne: "Ok") {
                        guard let vc = LaQueueStoryBoard.Authenticate(controller: Controller.RecoverCode) as? RecoverCodeVC else { return }
                        vc.isOtpVerify = true
                        vc.userId = isResponse.data?.register?.userID ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
