//
//  NetworkCheck.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 19/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class NetworkCheck: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //checking whether the user is logged in
        if LoginSession.shared.checkLoginStatus() {
            // Go to home user is logged in
            self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.TabBar), animated: true)
        } else {
            // Go to Login screen user is not logged in
            let nc = RootController.init(rootViewController: LaQueueStoryBoard.Authenticate(controller: Controller.Splash))
            nc.setNavigationBarHidden(true, animated: false)
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .fullScreen
            }
            self.navigationController?.present(nc, animated: false, completion: nil)
        }
    }

}
