//
//  LoginVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 20/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_businessCode: UITextField!
    @IBOutlet weak var viewSplash: UIView!
    
    var isFromSplash = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkUserLogin()
    }
    
    private func checkUserLogin() {
        if LoginSession.shared.checkLoginStatus() {
            // Go to home user is logged in
            viewSplash.isHidden = true
            if UserDefaults.standard.value(forKey: "profileStatus") as? Int == 0 {
                guard let vc = LaQueueStoryBoard.Menu(controller: Controller.UpdateProfile) as? UpdateProfileVC else {return}
                self.navigationController?.pushViewController(vc, animated: false)
            } else {
                let vc = LaQueueStoryBoard.Authenticate(controller: Controller.TabBar) as! TabBarVC
                self.navigationController?.pushViewController(vc, animated: false)
            }
        } else {
            viewSplash.isHidden = true
        }
    }
    
    // MARK:- Button Actions
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btn_backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_forgotPasswordTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.ForgotPassword), animated: true)
    }
    
    @IBAction func btn_loginTap(_ sender: UIButton) {
        if validateInputs() {
            loginUser()
        }
    }
    
    @IBAction func btn_signupTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.SignUp), animated: true)

//        if isFromSplash {
//            self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.SignUp), animated: true)
//        } else {
//            self.navigationController?.popViewController(animated: true)
//        }
    }
}

extension LoginVC {
    func validateInputs() -> Bool {
        var msg = ""
        if txt_email.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter an email address"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter a valid email address"
        } else if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your password"
        }
//        else if txt_businessCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
//            msg = "Please enter business code"
//        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func loginUser() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":UserStore.fcmtoken ?? "",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]

        let param:[String:Any] = [
            "email": txt_email.text ?? "",
            "password": txt_password.text ?? "",
            "type": "0"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.login, method: HTTPMethod.post, Param: param, header: header , type: LoginModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
//                        self.dismiss(animated: true, completion: nil)
                        // Go to home user is logged in
                        if isResponse.data?.login?.otp != nil {
                            guard let vc = LaQueueStoryBoard.Authenticate(controller: Controller.RecoverCode) as? RecoverCodeVC else {return}
                            vc.isOtpVerify = true
                            vc.userId = isResponse.data?.login?.userid ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            LoginSession.shared.updateLoginStatus(true)
                            LoginSession.shared.saveUserDefault(userModel: isResponse, keyName: userData)
                            UserStore.save(userPic: isResponse.data?.login?.userPic)
                            UserDefaults.standard.set(isResponse.data?.login?.userStatus, forKey: "profileStatus")
                            UserDefaults.standard.synchronize()
                            if UserDefaults.standard.value(forKey: "profileStatus") as? Int == 0 {
                                guard let vc = LaQueueStoryBoard.Menu(controller: Controller.UpdateProfile) as? UpdateProfileVC else {return}
                                vc.fromLogin = true
                                vc.email = isResponse.data?.login?.userEmail ?? ""
                                vc.profilePic = isResponse.data?.login?.userPic ?? ""
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.TabBar), animated: true)
                            }
                        }
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
