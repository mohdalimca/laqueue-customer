//
//  SplashOptionsVC.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 20/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SplashOptionsVC: UIViewController {

    @IBOutlet weak var stackButton: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stackButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //checking whether the user is logged in
        if LoginSession.shared.checkLoginStatus() {
            stackButton.isHidden = true
            // Go to home user is logged in
            if UserDefaults.standard.value(forKey: "profileStatus") as? Int == 0 {
                guard let vc = LaQueueStoryBoard.Menu(controller: Controller.UpdateProfile) as? UpdateProfileVC else {return}
                vc.fromLogin = true
                vc.email = "\(LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userEmail! ?? "")"
                vc.profilePic = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userPic ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.navigationController?.pushViewController(LaQueueStoryBoard.Authenticate(controller: Controller.TabBar), animated: true)
            }
        } else {
            stackButton.isHidden = false
//            // Go to Login screen user is not logged in
//            let nc = RootController.init(rootViewController: LaQueueStoryBoard.Authenticate(controller: Controller.Splash))
//            nc.setNavigationBarHidden(true, animated: false)
//            if #available(iOS 13.0, *) {
//                nc.isModalInPresentation = true
//                nc.modalPresentationStyle = .fullScreen
//            }
//            self.navigationController?.present(nc, animated: false, completion: nil)
        }
    }
    
    @IBAction func btn_loginTapped(_ sender: UIButton) {
       let vc = LaQueueStoryBoard.Authenticate(controller: "LoginVC") as! LoginVC
       vc.isFromSplash = true
       self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_signUpTp(_ sender: UIButton) {
        let vc = LaQueueStoryBoard.Authenticate(controller: Controller.SignUp) as! SignUpVC
        vc.isFromSplash = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
