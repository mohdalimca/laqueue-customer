//
//  AddressDetails.swift
//  Event
//
//  Created by cis on 30/12/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct AddressDetails {
    var zipcode : String
    var fullAddress : String
    var latitude : Float
    var longitude : Float
    var subLocality: String
    var locality: String
    
    init(zipcode:String, fullAddress:String, latitude:Float, longitude:Float, subLocality: String, locality: String) {
        self.zipcode = zipcode
        self.fullAddress = fullAddress
        self.latitude = latitude
        self.longitude = longitude
        self.subLocality = subLocality
        self.locality = locality
    }
}
