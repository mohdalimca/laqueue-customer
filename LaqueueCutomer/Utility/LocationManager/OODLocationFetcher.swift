//
//  OODLocationFetcher.swift
//  OrganizersOnDemand
//
//  Created by cis on 24/05/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import Foundation
import AVKit
import GooglePlaces

class OODLocationFetcher: NSObject, GMSAutocompleteFetcherDelegate {
    static let sharedInstance:OODLocationFetcher = OODLocationFetcher()
    private var fetcher: GMSAutocompleteFetcher?
    private var fetchPlace: GMSPlacesClient = GMSPlacesClient()
    private var returnAddress:(([GMSAutocompletePrediction])->Void)!

    //Functions
    func function_CreateSession() {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        self.fetcher = GMSAutocompleteFetcher(bounds: nil, filter: filter)
        let token: GMSAutocompleteSessionToken = GMSAutocompleteSessionToken.init()
        fetcher?.delegate = self
        fetcher?.provide(token)
    }
    func function_GetAddress(text: String?, success: @escaping ([(String, String)])->Void) {
        guard let isFetcher = self.fetcher else { return }
        isFetcher.sourceTextHasChanged(text ?? "")
        self.returnAddress = { (data) in
            var tempArr:[(String, String)] = []
            for item in data {
                tempArr.append((item.attributedFullText.string, item.placeID))
            }
            success(tempArr)
        }
    }
    func function_GetFetchPlace(placeID: String, success: @escaping (GMSPlace)->Void, isError: @escaping ()->Void) {
        if let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue)) {
            self.fetchPlace.fetchPlace(fromPlaceID: placeID, placeFields: fields, sessionToken: nil) { (place, error) in
                if let isPlace = place {
                    success(isPlace)
                } else {
                    isError()
                }
            }
        }
        
    }
    //Delegates
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.returnAddress(predictions)
    }
    func didFailAutocompleteWithError(_ error: Error) {
        self.returnAddress([])
    }
}
