//
//  MarkerInfoView.swift
//  LaqueueCutomer
//
//  Created by Apple on 12/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Foundation

class MarkerInfoView: UIView {
    
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblRetailer: UILabel!
    @IBOutlet weak var lblPerson: UILabel!
    @IBOutlet weak var lblWaitingTime: UILabel!
    @IBOutlet weak var btnShowDetail: UIButton!
    var didShowDetail: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    private func setup() {
        btnShowDetail.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        didShowDetail?()
    }
}
