//
//  UILabel+DynamicFontSize.h
//  Cook4Me
//
//  Created by cis on 18/07/18.
//  Copyright © 2018 cis. All rights reserved.
//

#import <Foundation/Foundation.h>
 #import <UIKit/UIKit.h>

@interface UILabel (DynamicFontSize)
@property (nonatomic) IBInspectable BOOL adjustFontSize;

@end
