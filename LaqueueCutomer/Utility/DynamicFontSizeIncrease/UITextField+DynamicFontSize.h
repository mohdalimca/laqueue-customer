//
//  UITextField+DynamicFontSize.h
//  Cook4Me
//
//  Created by cis on 20/07/18.
//  Copyright © 2018 cis. All rights reserved.
//

#ifndef UITextField_DynamicFontSize_h
#define UITextField_DynamicFontSize_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITextField (DynamicFontSize)
@property (nonatomic) IBInspectable BOOL adjustFontSize;

@end


#endif /* UITextField_DynamicFontSize_h */
