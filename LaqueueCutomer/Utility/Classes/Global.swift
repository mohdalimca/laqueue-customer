//
//  Global.swift
//  FastAide
//
//  Created by cis on 10/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit

var dev_Token = ""
var isSkipTapped = false
var userData = "LoginData"
//let GOOGLE_API_KEY = "AIzaSyBmVn0ILWu4y2FfZDqw6tI_LwpJgZEcmok" // old
let GOOGLE_API_KEY = "AIzaSyAQHwHJpmNMxENAfjGcsmcXxDzmvYoNm6I"
let GOOGLE_MAP_MARKER = UIImage(named: "mapMarker")
let securityToken: [String: String] = ["SecurityToken": "n2Y0Acd6Yq6oo21sGDVTALRFz"]

struct Constants {
    static let APP_NAME = "LaQueue"
    static let NO_INTERNET = "No Internet Connection"
    static let SOMETHING_WENT_WRONG = "Something went wrong"
    static let ENTER_EMAIL = "Enter E-mail address"
    static let ENTER_VALID_EMAIL = "Enter Valid E-mail address"
    static let ENTER_PASSWORD = "Enter Password"
    static let ENTER_FULLNAME = "Enter your full name"
    static let ENTER_VALID_NUMBER = "Enter valid mobile number"
    static let TICK_CHECKBOX = "Please Accept terms and condition"
    static let ENTER_OLD_PASSWORD = "Enter your current password"
    static let ENTER_NEW_PASSWORD = "Enter new password"
    static let ENTER_CONFIRM_PASSWORD = "Enter confirm password"
    static let MATCH_CONFIRM_PASSWORD = "Confirm password should be same as new password"
    static let ENTER_VALID_COMPLAIN = "Enter valid complain or suggestions"
    static let ALERT_OK_OPTION = "Ok"
}

struct Controller {
    static let Splash = "SplashOptionsVC"
    static let Login = "LoginVC"
    static let SignUp = "SignUpVC"
    static let ForgotPassword = "ForgotPasswordVC"
    static let Home = "HomeVC"
    static let Search = "SearchVC"
    static let Menu = "MenuVC"
    static let StaticContent = "StaticContentVC"
    static let Prefrences = "PrefrencesVC"
    static let NewPassword = "NewPasswordVC"
    static let RecoverCode = "RecoverCodeVC"
    static let MyProfile = "MyProfileVC"
    static let TabBar = "TabBarVC"
    static let UpdateProfile = "UpdateProfileVC"
    static let NewHome = "NewHomeVC"
    static let RetailerDetails = "RetailerDetailsVC"
    static let ChangePassword = "ChangePasswordVC"
    static let DatePicker = "DatePicker"
    static let TakePlace = "TakePlaceVC"
    static let PlaceRequest = "PlaceRequestVC"
    static let QueueVC = "QueueVC"
    static let Picker = "PickerViewController"
    static let Notification = "NotificationViewController"
}

struct Cell {
    static let Home = "HomeCell"
    static let Search = "SearchCell"
    static let Restaurant = "RestaurantCell"
    static let Category = "CategoryCell"
    static let Queue = "QueueCell"
    static let QueueList = "QueueListCell"
    static let NotificationCell = "NotificationTableViewCell"
}

