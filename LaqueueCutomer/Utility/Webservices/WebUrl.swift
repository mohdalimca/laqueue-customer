//
//  WebUrl.swift
//  FastAide
//
//  Created by cis on 21/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct WebUrl {
    static let baseUrl            = "http://knowledgerevise.com/Laqueue/webservices/"
    static let emailVerify        = baseUrl + "users/signup_otp"
    static let login              = baseUrl + "users/customer_login"
    static let registration       = baseUrl + "users/register"
    static let forgotPassword     = baseUrl + "users/forgotpassword"
    static let newPassword        = baseUrl + "users/reset_password"
    static let userProfile        = baseUrl + "users/get_profile"
    static let updateProfile      = baseUrl + "users/profile_update"
    static let changePassword     = baseUrl + "users/changepassword"
    static let logout             = baseUrl + "users/logout"
    static let privacy            = baseUrl + "static_page/privacy_policy"
    static let aboutUs            = baseUrl + "static_page/about_app"
    static let terms              = baseUrl + "static_page/terms_condition"
    static let contactus          = baseUrl + "contact-us"
    static let getprefrence       = baseUrl + "users/get_prefrence_setting"
    static let updatePrefrences   = baseUrl + "users/prefrence_update"
    static let resendOTP          = baseUrl + "users/resend_otp"
    static let menu               = baseUrl + "get-subcategories-product"
    static let home               = baseUrl + "users/dashboard"
    static let searchText         = baseUrl + "users/search"
    static let retailerDetail     = baseUrl + "users/business_detail"
    static let takePlace          = baseUrl + "queues/booking"
    static let categories         = baseUrl + "users/category"
    static let queueList          = baseUrl + "users/my_orders"
    static let placeInQueue       = baseUrl + "users/order_single"
    static let bookingStatus      = baseUrl + "queues/booking_status"
    static let notifications      = baseUrl + "users/notification_list"
}
