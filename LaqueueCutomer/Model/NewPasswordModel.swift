//
//  NewPasswordModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 28/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - NewPasswordModel
struct NewPasswordModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: JSONAny?
}
