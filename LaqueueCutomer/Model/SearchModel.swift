//
//  SearchModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 07/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - SearchModel
struct SearchModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: [SearchData]?
}

// MARK: - SearchData
struct SearchData: Codable {
    let totalcount: String?
    let record: [SearchRecord]?
}

// MARK: - SearchRecord
struct SearchRecord: Codable {
    let busiID, busiCode, busiName: String?
    let busiImg: String?
    let busiAddress, busiLat, busiLng, country: String?
    let city, busiDescription, people, waitingTime: String?

    enum CodingKeys: String, CodingKey {
        case busiID = "busi_id"
        case busiCode = "busi_code"
        case busiName = "busi_name"
        case busiImg = "busi_img"
        case busiAddress = "busi_address"
        case busiLat = "busi_lat"
        case busiLng = "busi_lng"
        case country, city
        case busiDescription = "busi_description"
        case people
        case waitingTime = "waiting_time"
    }
}
