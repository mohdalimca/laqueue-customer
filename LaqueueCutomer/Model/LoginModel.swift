//
//  LoginModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 28/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: LoginData?
}

// MARK: - LoginData
struct LoginData: Codable {
    let login: Login?
}

// MARK: - Login
struct Login: Codable {
    let authorization, userid, userEmail, firstname: String?
    let lastname: String?
    let userStatus: Int?
    let userRole: String?
    let userPic: String?
    let otp:JSONAny?

    enum CodingKeys: String, CodingKey {
        case authorization = "Authorization"
        case userid = "user_id"
        case userEmail = "user_email"
        case firstname, lastname
        case userStatus = "user_status"
        case userRole = "user_role"
        case userPic = "user_pic"
        case otp = "otp"
    }
}
