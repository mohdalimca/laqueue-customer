//
//  RetailersModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 07/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - RetailersModel
struct RetailersModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: [RetailersData]?
}

// MARK: - RetailersData
struct RetailersData: Codable {
    let totalcount: Int?
    let record: [Record]?
}

// MARK: - Record
struct Record: Codable {
    let busiID, busiCode, busiName: String?
    let busiImg: String?
    let busiAddress, country, city, busiDescription: String?

    enum CodingKeys: String, CodingKey {
        case busiID = "busi_id"
        case busiCode = "busi_code"
        case busiName = "busi_name"
        case busiImg = "busi_img"
        case busiAddress = "busi_address"
        case country, city
        case busiDescription = "busi_description"
    }
}
