//
//  ProfileModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 29/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - ProfileModel
struct ProfileModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let data: DataClass?
    let message: String?
}

// MARK: - DataClass
struct DataClass: Codable {
    let profile: Profile?
}

// MARK: - Profile
struct Profile: Codable {
    let id, userEmail, firstname, lastname, dob: String?
    let countryCode, userPhone, userRole, busiID: String?
    let busiName, busiDescription, busiWaitingTime, busiCode: String?
    let busiAddress, busiLat, busiLng, busiType: String?
    let busiImg, country, city, adminStatus: String?
    let userPic: String?

    enum CodingKeys: String, CodingKey {
        case id
        case dob
        case userEmail = "user_email"
        case firstname, lastname
        case countryCode = "country_code"
        case userPhone = "user_phone"
        case userRole = "user_role"
        case busiID = "busi_id"
        case busiName = "busi_name"
        case busiDescription = "busi_description"
        case busiWaitingTime = "busi_waiting_time"
        case busiCode = "busi_code"
        case busiAddress = "busi_address"
        case busiLat = "busi_lat"
        case busiLng = "busi_lng"
        case busiType = "busi_type"
        case busiImg = "busi_img"
        case country, city
        case adminStatus = "admin_status"
        case userPic = "user_pic"
    }
}
