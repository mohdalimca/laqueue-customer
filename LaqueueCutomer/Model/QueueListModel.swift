//
//  QueueListModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - QueueListModel
struct QueueListModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: [QueueListData]?
}

// MARK: - QueueListData
struct QueueListData: Codable {
    let totalcount: String?
    let record: [QueueRecord]?
}

// MARK: - QueueRecord
struct QueueRecord: Codable {
    let queName, callTimer, totalQueueNo, userID: String?
    let firstname, lastname: String?
    let userPic: String?
    let bookingID, queID, queSlotTime, message: String?
    let status, people, bookingTime: String?
    let schedule: [Schedule]?
    let queueStatus, waitingTime:String?

    enum CodingKeys: String, CodingKey {
        case queName = "que_name"
        case callTimer = "call_timer"
        case totalQueueNo = "total_queue_no"
        case userID = "user_id"
        case firstname, lastname
        case userPic = "user_pic"
        case bookingID = "booking_id"
        case queID = "que_id"
        case queSlotTime = "que_slot_time"
        case message, status, people
        case bookingTime = "booking_time"
        case schedule
        case queueStatus = "que_status"
        case waitingTime = "waiting_time"
    }
}


struct Schedule: Codable {
    let slotID, isOff, days, oneTo: String?
    let oneFrom, twoTo, twoFrom, isCustom: String?
    let customDate: String?

    enum CodingKeys: String, CodingKey {
        case slotID = "slot_id"
        case isOff = "is_off"
        case days
        case oneTo = "one_to"
        case oneFrom = "one_from"
        case twoTo = "two_to"
        case twoFrom = "two_from"
        case isCustom = "is_custom"
        case customDate = "custom_date"
    }
}

enum BookingStatus:String, Codable {
    case New = "New"
    case Acknowledge = "Acknowledge"
    case Noshow = "Noshow"
    case Call = "Call"
    case Update = "Update"
    case Accept = "Accept"
    case Cancel = "Cancel"
    case All = "All"
}
