//
//  RetailerDetailModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 07/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Foundation

// MARK: - RetailerDetailModel
struct RetailerDetailModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: RetailerDetailData?
}

// MARK: - RetailerDetailData
struct RetailerDetailData: Codable {
    let businessDetail: BusinessDetail?

    enum CodingKeys: String, CodingKey {
        case businessDetail = "business_detail"
    }
}

// MARK: - BusinessDetail
struct BusinessDetail: Codable {
    let busiID, busiCode, busiName: String?
    let busiImg: String?
    let busiAddress, busiContryCode, busiPhone, busiLat: String?
    let busiLng, country, city, busiDescription: String?
    let busiType: String?
    let queueDetails: [[QueueDetail]]?

    enum CodingKeys: String, CodingKey {
        case busiID = "busi_id"
        case busiCode = "busi_code"
        case busiName = "busi_name"
        case busiImg = "busi_img"
        case busiAddress = "busi_address"
        case busiContryCode = "busi_contry_code"
        case busiPhone = "busi_phone"
        case busiLat = "busi_lat"
        case busiLng = "busi_lng"
        case country, city
        case busiDescription = "busi_description"
        case busiType = "busi_type"
        case queueDetails = "queue_details"
    }
}

// MARK: - QueueDetail
struct QueueDetail: Codable {
    let queID, queName, callNextPersons, callNextPlaces: String?
    let callTimer, queDescription, capacity, totalQueueNo: String?
    let status: String?
    let slotInfo: [SlotInfo]?

    enum CodingKeys: String, CodingKey {
        case queID = "que_id"
        case queName = "que_name"
        case callNextPersons = "call_next_persons"
        case callNextPlaces = "call_next_places"
        case callTimer = "call_timer"
        case queDescription = "que_description"
        case capacity
        case totalQueueNo = "total_queue_no"
        case status
        case slotInfo = "slot_info"
    }
}

// MARK: - SlotInfo
struct SlotInfo: Codable {
    let isOff, days, queOneTo, queOneFrom: String?
    let queTwoTo, queTwoFrom, isCustom, customDate: String?
    let slotId: String?

    enum CodingKeys: String, CodingKey {
        case isOff = "is_off"
        case days
        case queOneTo = "que_one_to"
        case queOneFrom = "que_one_from"
        case queTwoTo = "que_two_to"
        case queTwoFrom = "que_two_from"
        case isCustom = "is_custom"
        case customDate = "custom_date"
        case slotId = "slot_id"
    }
}
